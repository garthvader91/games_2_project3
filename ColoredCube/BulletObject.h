#pragma once

#include "d3dUtil.h"
#include "GameObject.h"
#include "Box.h"
#include "Camera.h"
#include "constants.h"
#include "audio.h"


class BulletObject : public GameObject { 
public:
	void setDirection(Vector3 d) { direction = d; }
	void update(float);
private:
	Vector3 direction;
};