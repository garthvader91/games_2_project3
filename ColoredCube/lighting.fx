//=============================================================================
// lighting.fx by Frank Luna (C) 2008 All Rights Reserved.
//
// Transforms and lights geometry.
//=============================================================================

#include "lighthelper.fx"
 
cbuffer cbPerFrame
{
	Light gLight1;
	Light gLight2;
	Light gLight3;
	Light gLight4;
	Light gLight5;
		  

	int gLightType1;
	int gLightType2;
	float3 gEyePosW;
};

int ghostFlip;
int playerFlip;
int bulletColor;

cbuffer cbPerObject
{
	float4x4 gWorld;
	float4x4 gWVP; 
	float4x4 gTexMtx;
	float4 gReflectMtrl;
	bool gCubeMapEnabled;
};

// Nonnumeric values cannot be added to a cbuffer.
Texture2D gDiffuseMap;
Texture2D gSpecMap;
Texture2D gNormalMap;
TextureCube gCubeMap;

SamplerState gTriLinearSam
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = MIRROR;
	AddressV = MIRROR;
	//AddressU = Wrap;
	//AddressV = Wrap;
};

struct VS_IN
{
	float3 posL     : POSITION;
	float3 tangentL : TANGENT;
	float3 normalL  : NORMAL;
	float2 texC     : TEXCOORD;
};

struct VS_OUT
{
	float4 posH     : SV_POSITION;
    float3 posW     : POSITION;
    float3 tangentW : TANGENT;
    float3 normalW  : NORMAL;
    float2 texC     : TEXCOORD;
};

VS_OUT VS(VS_IN vIn)
{
	VS_OUT vOut;
	
	// Transform to world space space.
	vOut.posW    = mul(float4(vIn.posL, 1.0f), gWorld);
	vOut.tangentW = mul(float4(vIn.tangentL, 0.0f), gWorld);
	vOut.normalW  = mul(float4(vIn.normalL, 0.0f), gWorld);		
	// Transform to homogeneous clip space.
	vOut.posH = mul(float4(vIn.posL, 1.0f), gWVP);
	
	// Output vertex attributes for interpolation across triangle.
	//vOut.diffuse = vIn.diffuse;
	//vOut.spec    = vIn.spec;

	vOut.texC  = mul(float4(vIn.texC, 0.0f, 1.0f), gTexMtx);
	
	return vOut;
}
 
float4 PS(VS_OUT pIn) : SV_Target
{
   	float4 diffuse = gDiffuseMap.Sample( gTriLinearSam, pIn.texC );
	
	if (ghostFlip == 1) {
		diffuse = float4( (1 - diffuse.r)/2, (1 - diffuse.g)/2, (1 - diffuse.b)/2, diffuse.a);
	} 
	/*else if (flip == 2)
	{
		return float4(1 - diffuse.r, 1 - diffuse.g, 1 - diffuse.b, diffuse.a);
	}*/
	
	if (playerFlip != 0 && playerFlip > 0) {
		diffuse = float4(diffuse.r * (4 - playerFlip ), diffuse.gba );
	}

	if (bulletColor == 1) {
		diffuse = float4(0,diffuse.g * 5, diffuse.b * 5, diffuse.a);
	}
	
	if (bulletColor == 2) {
		diffuse = float4(diffuse.r * 3, 0, diffuse.b * 3, diffuse.a);
	}


		// Kill transparent pixels.
	clip(diffuse.a - 0.15f);

	float4 spec    = gSpecMap.Sample( gTriLinearSam, pIn.texC );
	float3 normalT = gNormalMap.Sample( gTriLinearSam, pIn.texC );

	// Map [0,1] --> [0,256]
	spec.a *= 256.0f;
	
	// Uncompress each component from [0,1] to [-1,1].
	normalT = 2.0f*normalT - 1.0f;
	
	// build orthonormal basis
	float3 N = normalize(pIn.normalW);
	float3 T = normalize(pIn.tangentW - dot(pIn.tangentW, N)*N);
	float3 B = cross(N,T);
	
	float3x3 TBN = float3x3(T, B, N);
	
	// Transform from tangent space to world space.
	float3 bumpedNormalW = normalize(mul(normalT, TBN));
    
	// Compute the lit color for this pixel.
    SurfaceInfo v = {pIn.posW, bumpedNormalW, diffuse, spec};
    
    float3 litColor = float3(0.0f, 0.0f, 0.0f);


	

    if ( gLightType1 == 0 ) // Parallel
    {
		litColor = ParallelLight(v, gLight1, gEyePosW);
		//litColor = dot(litColor, float3(0.3, 0.59, 0.11));
		litColor = dot(litColor, float3(1.0, 1.0, 1.0));
    }
    else if ( gLightType1 == 1 ) // Point
    {
		litColor  = PointLight(v, gLight1, gEyePosW);
		litColor += PointLight(v, gLight2, gEyePosW);
		litColor += PointLight(v, gLight3, gEyePosW);
		litColor += PointLight(v, gLight4, gEyePosW);
		litColor += PointLight(v, gLight5, gEyePosW);
	}
	else if ( gLightType1 == 2 ) // Spot
	{
		litColor = Spotlight(v, gLight1, gEyePosW);
		//litColor = dot(litColor, float3(0.0, 0.59, 0.41));
	}
	else 
	{
		litColor = ParallelLight(v, gLight1, gEyePosW);
		litColor = dot(litColor, float3(0.3, 0.59, 0.11));
	}

	litColor *= 0.4;
	litColor += 0.7 * ParallelLight(v,gLight1, gEyePosW);

	
    return float4(litColor, diffuse.a);
}

technique10 LightTech
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS() ) );
    }
}