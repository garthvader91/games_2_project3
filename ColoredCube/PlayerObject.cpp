
#include "PlayerObject.h"

PlayerObject::PlayerObject()
{
	collision = false;
	hasKey = false;
	resetHealth();
}

void PlayerObject::init(Turrent *b, float r, Vector3 pos, Vector3 vel, float sp, float s)
{
	GameObject::init(nullptr,r,pos,vel,sp,s);
	turrent = b;
	originalPos = pos;
}

void PlayerObject::draw()
{
	if (!getActiveState())
		return;
    D3D10_TECHNIQUE_DESC techDesc;
    GameObject::mTech->GetDesc( &techDesc );
    for(UINT p = 0; p < techDesc.Passes; ++p)
    {
        GameObject::mTech->GetPassByIndex( p )->Apply(0);
        turrent->draw();
    }
		/*box->draw();*/
}

void PlayerObject::update(float dt)
{
	GameObject::update(dt);
}

//sets the collision vector to be of the appropraite value
void PlayerObject::bounce(GameObject *go) {
	Vector3 collisionVector = go->getPosition() - getPosition();

	//normalize the collision vec and make it super small so that it's not shaky
	collisionVector = *D3DXVec3Normalize(&collisionVector, &collisionVector)/200;

	int count = 30;
	do {
		setPosition(getPosition() - collisionVector);
		count--;
	} while (GameObject::collided(go) && count);
}