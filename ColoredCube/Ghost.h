#pragma once

#include "d3dUtil.h"
#include "GameObject.h"
#include "Box.h"
#include "Camera.h"
#include "constants.h"
#include "point.h";
#include "PlayerObject.h"
#include "audio.h"


class Ghost : public GameObject
{
public:
	Ghost();
	void update(float dt);
	void init(TexBox *b, float r, Vector3 pos, Vector3 vel, float sp, float s, PlayerObject* player);
	void setDirection(Vector3 v) { direction = v; }

	int getHealth() { return health; }
	void decrementHealth() { health--; }
	void resetHealth() { health = 2; }
	void setAudio(Audio* a) {audio = a;}
	void bounce(GameObject *go);
private:
	void vectorTrack();
	PlayerObject* player;
	Vector3 direction;
	Audio* audio;

	int health;
};