#pragma once

#include <time.h>

class TimeBuffer {
private:
	unsigned long begTime;
	float waitTime;
public:
	TimeBuffer() { waitTime = 0.0f; }
	TimeBuffer(float w) { waitTime = w; }
	void resetClock(float waitTime) {
		begTime = clock();
		this->waitTime = waitTime;
	}

	bool waitTimeIsOver() {
		return elapsedTime() > waitTime;
	}

	float elapsedTime() {
		return  ( ((float) clock() - (begTime)) / (float)CLOCKS_PER_SEC);
	}


	/*bool canFire(unsigned long seconds) {

	return seconds <= elapsedTime();
	}*/


};