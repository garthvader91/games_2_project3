//=============================================================================
// Color Cube App.cpp by Frank Luna (C) 2008 All Rights Reserved.
//
// Demonstrates coloring.
//
// Controls:
//		'A'/'D'/'W'/'S' - Rotate 
//
//=============================================================================

#include "d3dApp.h"
#include "Box.h"
#include "mapMaker.h"
#include "Line.h"
#include "Axes.h"
#include "Vertex.h"
#include "Triangle.h"
#include "Quad.h"
#include "Input.h"
#include "GameObject.h"
#include "PlayerObject.h"
#include "audio.h"
#include "winerror.h"
#include "TimeBuffer.h"
#include "Ghost.h"
#include "BulletObject.h"
#include "Camera.h"
#include "point.h"
#include "Light.h"
#include "Effects.h"
#include "InputLayouts.h"
#include "Sky.h"
#include <sstream>
#include "Turrent.h"


//Vector3 PLAYER_START_LVL1 = Vector3(10, 0, 10);
Vector3 PLAYER_START_LVL1 = Vector3(19, 0, 19);
Vector3 INITIAL_LOOKAT    = Vector3(0,0,19);

class ColoredCubeApp : public D3DApp
{

public:
	ColoredCubeApp(HINSTANCE hInstance);
	~ColoredCubeApp();

	void initApp();
	void onResize();
	void updateScene(float dt);
	void drawScene();
	void restartGame();
	bool lightIsOnGhost(Ghost* ghost);
	void getMazeDimensions(mapMaker maze);
	void buildMaze(mapMaker maze);
	void goBackToStart();
	void runExplosion(Vector3 pos);

	bool isTowardTop();
	bool isTowardLeft();
	bool isTowardRight();
	bool isTowardBottom();

	bool isGhostInTop();
	bool isGhostInRight();
	bool isGhostInBottom();
	bool isGhostInLeft();
	string getTopThreeHighScores();
	void storeScore();
	void setAlerts();

private:

	void buildFX();
	void buildVertexLayouts();

	Input* input;
	Audio* audio;

	TexBox mPlayer;
	TexBox mBox;
	TexBox bulletBox;
	TexBox floorBox;
	TexBox particleBox;
	TexBox enemyBox;
	TexBox alertBox;
	TexBox backAlertBox;

	Turrent mTurrent;

	GameObject *particles;
	PlayerObject playerObject;
	GameObject floor;

	GameObject* mazeBlocks;
	GameObject* mazeBlocksTall;
	GameObject* mazeBlocksTaller;
	GameObject alertBlocks[3];
	GameObject blockerBlocks[12];

	Sky mSky;
	///////ENEMY STUFF////////////
	Ghost enemyBoxes[MAX_ENEMIES];
	Vector3 *spawnPositions;
	Vector3 *spawnPositionsTwo;
	void generateEnemies();
	int nextEnemy;
	TimeBuffer enemyTimeBuffer;
	//////////////////////////////


	///////////BULLET STUFF//////////
	BulletObject bullets[MAX_BULLETS];
	void fireBullet();
	int nextBullet;
	TimeBuffer shotTimeBuffer;
	/////////////////////////////////

	Light mLights[5];
	int mLightType; // 0 (parallel), 1 (point), 2 (spot)

	mapMaker maze1;

	//ID3D10Effect* mFX;
	ID3D10EffectTechnique* mTech;
	//ID3D10InputLayout* mVertexLayout;
	ID3D10EffectMatrixVariable* mfxWVPVar;

	///////////Lighting///////////
	ID3D10EffectMatrixVariable* mfxWorldVar;
	ID3D10EffectVariable* mfxEyePosVar;
	ID3D10EffectVariable* mfxFlashLightVar;
	ID3D10EffectVariable* mfxFlashLightVar2;
	ID3D10EffectVariable* mfxFlashLightVar3;
	ID3D10EffectVariable* mfxFlashLightVar4;
	ID3D10EffectVariable* mfxFlashLightVar5;
	///////////////////////////////////////
	ID3D10EffectVariable* mfxSpotLightVar;
	ID3D10EffectVariable* mfxFlipGhostVar;
	ID3D10EffectVariable* mfxBulletColorVar;
	ID3D10EffectVariable* mfxFlipPlayerVar;
	ID3D10EffectScalarVariable* mfxLightType;

	///////////////////////////////

	///////////Texture stuff//////
	ID3D10ShaderResourceView* mStoneMapRV;
	ID3D10ShaderResourceView* mDiffuseMapRV;
	ID3D10ShaderResourceView* floorTex;
	ID3D10ShaderResourceView* ghostTex;
	ID3D10ShaderResourceView* doorTex;
	ID3D10ShaderResourceView* keyTex;
	ID3D10ShaderResourceView* stoneTex;
	ID3D10ShaderResourceView* grassTex;
	ID3D10ShaderResourceView* cityTex;
	ID3D10ShaderResourceView* roadTex;
	ID3D10ShaderResourceView* tallerTex;
	ID3D10ShaderResourceView* tallTex;
	ID3D10ShaderResourceView* fireTex;

	ID3D10ShaderResourceView* mEnvMapRV;
	//////////////////////////////

	///////////NORMAL MAP////////
	ID3D10EffectShaderResourceVariable* mfxNormalMapVar;
	ID3D10ShaderResourceView* mfxBuildingNormalMapRV;
	ID3D10ShaderResourceView* defaultNormalMapRV;

	ID3D10EffectShaderResourceVariable* mfxCubeMapVar;
	/////////////////////////////////////////////////////


	ID3D10ShaderResourceView* mSpecMapRV;

	ID3D10EffectShaderResourceVariable* mfxDiffuseMapVar;
	ID3D10EffectShaderResourceVariable* mfxSpecMapVar;
	ID3D10EffectMatrixVariable* mfxTexMtxVar;

	D3DXMATRIX mView;
	D3DXMATRIX mProj;
	D3DXMATRIX mWVP;

	// Camera stuff
	Camera camera;
	Vector3 cameraPos;
	Vector3 lookAt;

	float mTheta;
	float mPhi;

	int score;
	int scoreAndBonus;
	std::wstring finalScore;
	std::wstring gunType;
	std::wstring gameOverString;
	std::wstring timeString;

	int maze1Height;
	int maze1Width;
	int blockCounter;
	int blockCounterTall;
	int blockCounterTaller;
	int enemySpawnPointsCounter;
	// Timer
	float timer;
	float level2Timer;
	float level3Timer;

	float explosionTimer;
	bool explosionRunning;

	bool gameOver;


#pragma region guns
	enum GUNS { NORMAL, POWER, BURST };
	GUNS* gunArray;
	GUNS gunSelector;
	int burstCounter;

	void changeGun();
	bool LShiftDownLastFrame;
	bool gunChanged;
	float gunChangedViewTimer;
	bool viewGunChanged;
	float burstCoolTimer;
#pragma endregion

	bool didWin;
	bool level1;
	bool onLevelOneMenu;
	bool level2;
	bool onLevelTwoMenu;
	bool level3;
	bool onLevelThreeMenu;
	bool onHighscoreScreen;
	float shotsHit;
	float shotsFired;
	int accuracyBonus;

	int enemyKillCount;

};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif


	ColoredCubeApp theApp(hInstance);

	theApp.initApp();

	return theApp.run();
}

ColoredCubeApp::ColoredCubeApp(HINSTANCE hInstance) : D3DApp(hInstance), mTech(0), 
	mfxWVPVar(0), mSpecMapRV(0), mDiffuseMapRV(0), 
	ghostTex(0), mfxTexMtxVar(0),floorTex(0), mfxDiffuseMapVar(0), 
	mfxSpecMapVar(0), mTheta(0.0f), mPhi(PI*0.25f), mStoneMapRV(0), 
	cityTex(0), roadTex(0), tallerTex(0), keyTex(0), tallTex(0), mfxNormalMapVar(0), mfxBuildingNormalMapRV(0)
	, defaultNormalMapRV(0)
{
	D3DXMatrixIdentity(&mView);
	D3DXMatrixIdentity(&mProj);
	D3DXMatrixIdentity(&mWVP); 

	input = new Input();
	audio = new Audio();
}

ColoredCubeApp::~ColoredCubeApp()
{
	if( md3dDevice )
		md3dDevice->ClearState();

	ReleaseCOM(mfxBuildingNormalMapRV);
	ReleaseCOM(defaultNormalMapRV);
	//ReleaseCOM(mFX);
	//ReleaseCOM(mVertexLayout);
	//safeDelete(input);
	ReleaseCOM(mStoneMapRV);
	ReleaseCOM(mDiffuseMapRV);
	ReleaseCOM(floorTex);
	ReleaseCOM(ghostTex);
	ReleaseCOM(stoneTex);
	ReleaseCOM(grassTex);
	ReleaseCOM(cityTex);
	ReleaseCOM(roadTex);
	ReleaseCOM(tallerTex);
	ReleaseCOM(tallTex);
	ReleaseCOM(keyTex);
	ReleaseCOM(fireTex);

}

void ColoredCubeApp::initApp()
{

	D3DApp::initApp();
	cameraPos = Vector3(PLAYER_START_LVL1); // will translate (2.7, 1.8, 0)
	camera.init(cameraPos, camera.getDirection(), INITIAL_LOOKAT);

	//for the player and fist-person camera --- shouldn't be drawn, just for collision detection
	mPlayer.init(md3dDevice, 0.5f, 0);
	enemyBox.init(md3dDevice, 0.5f, 0);
	mBox.init(md3dDevice, 1.0f, 0);
	bulletBox.init(md3dDevice, 0.1f, 0);
	particleBox.init(md3dDevice, 0.0175f, 0);
	alertBox.init(md3dDevice, 0.15f, 0);
	backAlertBox.init(md3dDevice, 0.15f, 1);

	//PLAYER_START_LVL1

	mTurrent.init(md3dDevice, 0.7);

	playerObject.init(&mTurrent, 1.55f, Vector3(PLAYER_START_LVL1), Vector3(0,0,0), 5.0f, 1.0f);
	camera.setPerspective();

	for (int i = 0; i < MAX_BULLETS; i++)
	{
		bullets[i].init(&bulletBox, 0.5f, Vector3(0,0,0), Vector3(0,0,0), BULLET_SPEED, 1.0f);
		bullets[i].setInActive();
	}

	particles = new GameObject[MAX_NUM_PARTICLES];
	for (int i = 0; i < MAX_NUM_PARTICLES; i++){
		particles[i].init(&particleBox, 1.0, Vector3(0,0,0), Vector3(0,0,0), 0.4f, 1.0f);
		particles[i].setInActive();
	}

	for (int i = 0; i < 12; i++)
	{
		blockerBlocks[i].init(&mBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
		blockerBlocks[i].setInActive();
	}


	alertBlocks[0].init(&backAlertBox, 1.0, Vector3(21.1,.5,19), Vector3(0,0,0), 0.0f, 1.0f);
	alertBlocks[0].setActive();
	alertBlocks[1].init(&alertBox, 1.0, Vector3(19.25,1.0,17.75), Vector3(0,0,0), 0.0f, 1.0f);
	alertBlocks[1].setActive();
	alertBlocks[2].init(&alertBox, 1.0, Vector3(19.25,1.0,20.25), Vector3(0,0,0), 0.0f, 1.0f);
	alertBlocks[2].setActive();


	shotTimeBuffer.resetClock(0.0f);
	nextBullet = 0;
	explosionTimer = 0;
	explosionRunning = false;

	enemyKillCount = 0;

	fx::InitAll(md3dDevice);
	InputLayout::InitAll(md3dDevice);

	buildFX();
	//buildVertexLayouts();

#pragma region Textures
	mClearColor = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);


	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"StarryNight.dds", 0, 0, &mEnvMapRV, 0 ));
	mSky.init(md3dDevice, mEnvMapRV, 5000.0f);

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"defaultspec.dds", 0, 0, &mSpecMapRV, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"default_normal.dds", 0, 0, &defaultNormalMapRV, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"bumpmap.dds", 0, 0, &mfxBuildingNormalMapRV, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"floor.dds", 0, 0, &floorTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"ghost.dds", 0, 0, &ghostTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"Stone.dds", 0, 0, &stoneTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"alert.dds", 0, 0, &grassTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"wall.dds", 0, 0, &cityTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"road.dds", 0, 0, &roadTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"taller.dds", 0, 0, &tallerTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"tall.dds", 0, 0, &tallTex, 0 ));

	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice, 
		L"RedTiger.dds", 0, 0, &keyTex, 0 ));
	HR(D3DX10CreateShaderResourceViewFromFile(md3dDevice,
		L"fire.dds", 0, 0, &fireTex, 0 ));
#pragma endregion



#pragma region lightInitialization
	mLightType = 1;
	// Parallel light.
	mLights[0].dir      = D3DXVECTOR3(0, -1.0, 0.0f);

	mLights[0].ambient  = D3DXCOLOR(0.19f, 0.11f, 0.7f, 1.0f);
	//mLights[0].ambient  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mLights[0].diffuse  = D3DXCOLOR(0.3f, 0.59f, 0.11f, 1.0f);
	//mLights[0].diffuse  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mLights[0].specular = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

	mLights[0].pos = PLAYER_START_LVL1 + Vector3(0,5,0);

	for (int i = 0; i < 5; i++)
	{

		mLights[i].ambient  = D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.0f);
		mLights[i].diffuse  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		mLights[i].specular = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);

		/*mLights[i].ambient  = D3DXCOLOR(.2f, .2f, .2f, 1.0f);
		mLights[i].diffuse  = D3DXCOLOR(.8f, .8f, .8f, 1.0f);
		mLights[i].specular = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);*/
		/*mLights[i].ambient  = D3DXCOLOR(.5f, 0, 0 , 1.0f);
		mLights[i].diffuse  = D3DXCOLOR(0.5f, 0.5, 0.5, 1.0f);
		mLights[i].specular = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);*/

		mLights[i].att.x = 0.1f;
		mLights[i].att.y = 0.1f;
		mLights[i].att.z = 0.1f;
		mLights[i].range = 15.0f;
		mLights[i].pos   = PLAYER_START_LVL1; //+ Vector3(0,5,15);
	}

	mLights[1].pos += Vector3(0,0,15);
	mLights[2].pos += Vector3(15,0,0);
	mLights[3].pos += Vector3(-15,0,0);
	mLights[4].pos += Vector3(0,0,-15);
#pragma endregion

	audio = new Audio();
	if (*WAVE_BANK != '\0' && *SOUND_BANK != '\0')  // if sound files defined
	{
		if (!audio->initialize()) {

		}
		/*if( FAILED( hr = audio->initialize() ) )
		{
		if( hr == HRESULT_FROM_WIN32( ERROR_FILE_NOT_FOUND ) )
		throw(GameError(gameErrorNS::FATAL_ERROR, "Failed to initialize sound system because media file not found."));
		else
		throw(GameError(gameErrorNS::FATAL_ERROR, "Failed to initialize sound system."));
		}*/
	}


	audio->playCue(CITY_MUSIC);

	blockCounter = 0;
	blockCounterTall = 0;
	blockCounterTaller = 0;
	enemySpawnPointsCounter = 0;
	mapMaker maze1("map.txt");
	maze1Height = maze1.getHeight();
	maze1Width  = maze1.getWidth();

	floorBox.init(md3dDevice, maze1Width, 0);

	floor.init(&floorBox, sqrt(2.0f), Vector3(maze1Width - 1,-maze1Width - 1,maze1Width - 1), Vector3(0,0,0), 0, 1.0f);
	floor.setActive();

#pragma region mapStuff
	for (int i = 0; i < maze1Height; i++) {
		for (int j = 0; j < maze1Width; j++)
		{
			int index = (i * maze1Width) + j;

			if (maze1.getMap()[index] == 'w')
				blockCounter++;
			if (maze1.getMap()[index] == 't')
				blockCounterTall += 2;
			if (maze1.getMap()[index] == 'T')
				blockCounterTaller += 3;
			if (maze1.getMap()[index] == 'e' || maze1.getMap()[index] == 'E')
				enemySpawnPointsCounter++;		
		}
	}

	//allocate enemy spawn points
	spawnPositions = new Vector3[enemySpawnPointsCounter/2];
	spawnPositionsTwo = new Vector3[enemySpawnPointsCounter];
	////
	mazeBlocks = new GameObject[blockCounter];
	mazeBlocksTall = new GameObject[blockCounterTall];
	mazeBlocksTaller = new GameObject[blockCounterTaller];

	for (int i = 0; i < blockCounter; i++) {
		mazeBlocks[i].init(&mBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
		mazeBlocks[i].setInActive();
	}
	for (int i = 0; i < blockCounterTall; i++) {
		mazeBlocksTall[i].init(&mBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
		mazeBlocksTall[i].setInActive();
	}
	for (int i = 0; i < blockCounterTaller; i++) {
		mazeBlocksTaller[i].init(&mBox, sqrt(2.0f), Vector3(0,0,0), Vector3(0,0,0), 0, 1.0f);
		mazeBlocksTaller[i].setInActive();
	}

	Vector3 currentPosition = Vector3(0, 0, 0);
	int currentBlock = 0;
	int currentBlockTall = 0;
	int currentBlockTaller = 0;
	int currentSpawnPoint = 0;
	int currentSpawnPointTwo = 0;
	int blockBlocksCount = 0;
	for (int i = 0; i < maze1Height; i++) {
		for (int j = 0; j < maze1Width; j++) {
			if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'w'){
				mazeBlocks[currentBlock].setActive();
				mazeBlocks[currentBlock].setPosition(currentPosition);
				currentBlock++;
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 't'){
				mazeBlocksTall[currentBlockTall].setActive();
				mazeBlocksTall[currentBlockTall].setPosition(currentPosition);
				currentBlockTall++;
				mazeBlocksTall[currentBlockTall].setActive();
				mazeBlocksTall[currentBlockTall].setPosition(Vector3(currentPosition.x, currentPosition.y + 2.0f, currentPosition.z));
				currentBlockTall++;
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'T'){
				mazeBlocksTaller[currentBlockTaller].setActive();
				mazeBlocksTaller[currentBlockTaller].setPosition(currentPosition);
				currentBlockTaller++;
				mazeBlocksTaller[currentBlockTaller].setActive();
				mazeBlocksTaller[currentBlockTaller].setPosition(Vector3(currentPosition.x, currentPosition.y + 2.0f, currentPosition.z));
				currentBlockTaller++;
				mazeBlocksTaller[currentBlockTaller].setActive();
				mazeBlocksTaller[currentBlockTaller].setPosition(Vector3(currentPosition.x, currentPosition.y + 4.0f, currentPosition.z));
				currentBlockTaller++;
			}
			else if(maze1.getMap()[ ( i * maze1Width ) +j ] == 'B'){
				blockerBlocks[blockBlocksCount].setActive();
				blockerBlocks[blockBlocksCount].setPosition(currentPosition);
				blockBlocksCount++;
				blockerBlocks[blockBlocksCount].setActive();
				blockerBlocks[blockBlocksCount].setPosition(Vector3(currentPosition.x, currentPosition.y + 2.0f, currentPosition.z));
				blockBlocksCount++;
				blockerBlocks[blockBlocksCount].setActive();
				blockerBlocks[blockBlocksCount].setPosition(Vector3(currentPosition.x, currentPosition.y + 4.0f, currentPosition.z));
				blockBlocksCount++;
			}
			else if (maze1.getMap()[ i * maze1Width + j] == 'e') {
				spawnPositions[currentSpawnPoint] = currentPosition;
				spawnPositionsTwo[currentSpawnPointTwo] = currentPosition;
				currentSpawnPoint++;
				currentSpawnPointTwo++;
			}
			else if (maze1.getMap()[ i * maze1Width + j] == 'E') {
				spawnPositionsTwo[currentSpawnPointTwo] = currentPosition;
				currentSpawnPointTwo++;
			}
			currentPosition.z += 2.0f;

		}
		currentPosition.z = 0.0f;
		currentPosition.x += 2.0f;
	}
#pragma endregion mapstuff


	for (int i = 0; i < MAX_ENEMIES; i++)
	{
		enemyBoxes[i].init(&enemyBox, 0.5f, Vector3(0,0,0), Vector3(0,0,0), 2.0f, 0.0f, &playerObject);
		enemyBoxes[i].setInActive();
	}
	enemyTimeBuffer.resetClock(2.0f);
	nextEnemy = 0;


	timer = 60.0f;
	level2Timer = 30.0f;
	level3Timer = 45.0f;

	score = 0;
	scoreAndBonus = 0;
	gameOver = false;
	didWin = false;
	level1 = false;
	onLevelOneMenu = true;
	level2 = false;
	onLevelTwoMenu = false;
	level3 = false;
	onLevelThreeMenu = false;
	shotsFired = 0;
	shotsHit = 0;
	accuracyBonus = 0;
	onHighscoreScreen = false;
#pragma region guns
	gunArray = new GUNS[3];
	gunArray[0] = GUNS::NORMAL;
	gunArray[1] = GUNS::POWER;
	gunArray[2] = GUNS::BURST;

	gunSelector = GUNS::NORMAL;
	burstCounter = 0;
	LShiftDownLastFrame = false;
	gunChanged = false;
	gunChangedViewTimer = 0.0f;
	viewGunChanged = false;
	burstCoolTimer = 1.0f;
#pragma endregion

}

void ColoredCubeApp::onResize()
{
	D3DApp::onResize();
	//Camera Object
	camera.setPerspective();
	float aspect = (float)mClientWidth/mClientHeight;
	D3DXMatrixPerspectiveFovLH(&mProj, 0.25f*PI, aspect, 1.0f, 1000.0f);
}


void ColoredCubeApp::generateEnemies() {

	if(level1){
		if (enemyTimeBuffer.waitTimeIsOver()) {
			//pick a random spot out of the ones we decided to set as spawn points
			int posIndex = rand() % (enemySpawnPointsCounter/2);

			enemyBoxes[nextEnemy].setPosition(spawnPositions[posIndex]);
			enemyBoxes[nextEnemy].setActive();
			enemyBoxes[nextEnemy].resetHealth();
			enemyTimeBuffer.resetClock(2.75f);
			nextEnemy++;
			if (nextEnemy == MAX_ENEMIES)
				nextEnemy = 0;
		}
	}
	if(level2){
		if (enemyTimeBuffer.waitTimeIsOver()) {
			//pick a random spot out of the ones we decided to set as spawn points
			int posIndex = rand() % enemySpawnPointsCounter;

			enemyBoxes[nextEnemy].setPosition(spawnPositionsTwo[posIndex]);
			enemyBoxes[nextEnemy].setActive();
			enemyBoxes[nextEnemy].resetHealth();
			enemyTimeBuffer.resetClock(2.5f);
			nextEnemy++;
			if (nextEnemy == MAX_ENEMIES)
				nextEnemy = 0;
		}
	}
	if(level3){
		if (enemyTimeBuffer.waitTimeIsOver()) {
			//pick a random spot out of the ones we decided to set as spawn points
			int posIndex = rand() % enemySpawnPointsCounter;

			enemyBoxes[nextEnemy].setPosition(spawnPositionsTwo[posIndex]);
			enemyBoxes[nextEnemy].setActive();
			enemyBoxes[nextEnemy].resetHealth();
			enemyTimeBuffer.resetClock(2.25f);
			nextEnemy++;
			if (nextEnemy == MAX_ENEMIES)
				nextEnemy = 0;
		}
	}

}


void ColoredCubeApp::fireBullet() {

	//Check if can fire yet
	if (shotTimeBuffer.waitTimeIsOver()) {

		//position the bullet infront of the player
		Vector3 fireDirection = Vector3(-1,0,0);
		Transform(&fireDirection, &fireDirection, &camera.getMatrixYaw()); 

		//Vector3 fireDirection = camera.getLookAt(); /*- playerObject.getPosition();*/
		Normalize(&fireDirection, &fireDirection);

		bullets[nextBullet].setPosition( playerObject.getPosition() + fireDirection/2);
		bullets[nextBullet].setDirection(fireDirection);
		bullets[nextBullet].setActive();

		shotsFired++;
		audio -> playCue(SHOT);

		if (gunSelector == GUNS::BURST) {
			burstCoolTimer = 1.0f;
			burstCounter++;

			if (burstCounter < 3)
				shotTimeBuffer.resetClock(0.2f);

			else {
				shotTimeBuffer.resetClock(burstCoolTimer);
				burstCounter = 0;
			}
		}

		else if (gunSelector == GUNS::POWER)
			shotTimeBuffer.resetClock(1.2f);

		else shotTimeBuffer.resetClock(0.7f);

		nextBullet++;
		if (nextBullet == MAX_BULLETS)
			nextBullet = 0;
	}
}

void ColoredCubeApp::changeGun() {
	if (level1) { // cant toggle guns on level1
		gunSelector = GUNS::NORMAL;
		return;
	}

	if (level2) {
		if (gunSelector == GUNS::NORMAL) {
			gunChanged = true;
			viewGunChanged = true;
			gunSelector = GUNS::BURST;
		}

		else if (gunSelector == GUNS::BURST) {
			gunChanged = true;
			viewGunChanged = true;
			gunSelector = GUNS::NORMAL;
		}

		return;
	}

	if (level3) {

		if (gunSelector == GUNS::NORMAL) {
			gunChanged = true;
			viewGunChanged = true;
			gunSelector = GUNS::BURST;
		}

		else if (gunSelector == GUNS::BURST) {
			gunChanged = true;
			viewGunChanged = true;
			gunSelector = GUNS::POWER;
		}

		else if (gunSelector == GUNS::POWER) {
			gunChanged = true;
			viewGunChanged = true;
			gunSelector = GUNS::NORMAL;
		}

		return;
	}	
}

void ColoredCubeApp::updateScene(float dt)
{
#pragma region guns

	if (gunSelector == GUNS::BURST) {
		burstCoolTimer -= dt;
		if (burstCoolTimer < 0.2f) {
			burstCoolTimer = 0.2f;
			burstCounter = 0;
		}
	}


	gunChanged = false;

	if (viewGunChanged)
		gunChangedViewTimer += dt;

	if (gunChangedViewTimer > 3.0f) {
		viewGunChanged = false;
		gunChangedViewTimer = 0.0f;
	}


	if (GetAsyncKeyState(VK_UP) & 0x8000) {
		if (LShiftDownLastFrame == false) {
			changeGun();
			LShiftDownLastFrame = true;
		}
	}

	else LShiftDownLastFrame = false;

#pragma endregion

	if(GetAsyncKeyState(VK_SPACE) & 0x8000)
		fireBullet();
	generateEnemies();
	drawScene();

	D3DApp::updateScene(dt);

	D3DXMATRIX w;
	if(!gameOver && (level1 || level2 || level3)){
		for(int i = 0; i < blockCounter; i++)
			mazeBlocks[i].update(dt);
		for(int i = 0; i < blockCounterTall; i++)
			mazeBlocksTall[i].update(dt);
		for(int i = 0; i < blockCounterTaller; i++)
			mazeBlocksTaller[i].update(dt);

		floor.update(dt);

		for (int i = 0; i < MAX_ENEMIES; i++)
			enemyBoxes[i].update(dt);
		for (int i = 0; i < MAX_BULLETS; i++)
			bullets[i].update(dt);
		for (int i = 0; i < 3; i++)
			alertBlocks[i].update(dt);
		for (int i = 0; i < 12; i++)
			blockerBlocks[i].update(dt);

		playerObject.update(dt);

		for (int i = 0; i < MAX_BULLETS; i++)
		{
			for (int j = 0; j < MAX_ENEMIES; j++)
			{
				if (bullets[i].collided(&enemyBoxes[j])) {
					bullets[i].setInActive();

					enemyBoxes[j].decrementHealth();

					if (gunSelector == GUNS::POWER) // one shot kill with power gun
						enemyBoxes[j].decrementHealth();

					enemyBoxes[j].setSpeed(enemyBoxes[j].getSpeed()/ 1.75f);
					if (enemyBoxes[j].getHealth() == 0)
					{
						enemyBoxes[j].setSpeed(2.0f);
						enemyBoxes[i].resetHealth();
						enemyBoxes[j].setInActive();
						if(level1) enemyKillCount++;
					}

					score += 10;
					audio -> playCue(BOOM2);
					shotsHit++;

					explosionTimer = 0;
					runExplosion(bullets[i].getPosition());
				}
			}
		}

		//Bullets colliding with short walls
		for (int i = 0; i < MAX_BULLETS; i++)
		{
			for (int j = 0; j < blockCounter; j++)
			{
				if (bullets[i].collided(&mazeBlocks[j])) {
					bullets[i].setInActive();

					audio -> playCue(BOOM2);

					explosionTimer = 0;
					runExplosion(bullets[i].getPosition());
				}
			}
		}

		//Bullets colliding with tall walls
		for (int i = 0; i < MAX_BULLETS; i++)
		{
			for (int j = 0; j < blockCounterTall; j++)
			{
				if (bullets[i].collided(&mazeBlocksTall[j])) {
					bullets[i].setInActive();

					audio -> playCue(BOOM2);

					explosionTimer = 0;
					runExplosion(bullets[i].getPosition());
				}
			}
		}

		//Bullets colliding with taller walls
		for (int i = 0; i < MAX_BULLETS; i++)
		{
			for (int j = 0; j < blockCounterTaller; j++)
			{
				if (bullets[i].collided(&mazeBlocksTaller[j])) {
					bullets[i].setInActive();

					audio -> playCue(BOOM2);

					explosionTimer = 0;
					runExplosion(bullets[i].getPosition());
				}
			}
		}

		//Bullets colliding with blocker
		if(level1)
		{
			for (int i = 0; i < MAX_BULLETS; i++)
			{
				for (int j = 0; j < 12; j++)
				{
					if (bullets[i].collided(&blockerBlocks[j])) {
						bullets[i].setInActive();

						audio -> playCue(BOOM2);

						explosionTimer = 0;
						runExplosion(bullets[i].getPosition());
					}
				}
			}
		}

		for (int j = 0; j < MAX_ENEMIES; j++)
		{
			if (playerObject.collided(&enemyBoxes[j])) {
				playerObject.decrementHealth();
				if (playerObject.getHealth() == 0){
					playerObject.resetHealth();
					//score += accuracyBonus;
					scoreAndBonus = score + accuracyBonus;
					gameOver = true;
					storeScore();
				}

				enemyBoxes[j].resetHealth();
				enemyBoxes[j].setInActive();
				enemyBoxes[j].setSpeed(2.0f);
			}
		}

		for (int i = 0; i < MAX_NUM_PARTICLES; i++) particles[i].update(dt);

		if(explosionRunning) explosionTimer += dt;
		if (explosionTimer > 0.55f) {
			explosionTimer = 0;
			explosionRunning = false;
			for (int i = 0; i < MAX_NUM_PARTICLES; i++)
			{
				particles[i].setInActive();
			}
		}

		/*isTowardTop();
		isTowardLeft();
		isTowardRight();
		isTowardBottom();*/

		/*isGhostInTop();
		isGhostInRight();
		isGhostInBottom();
		isGhostInLeft();*/

		setAlerts();

		if(enemyKillCount == 10){

			for(int i = 0; i < MAX_ENEMIES; i++){
				enemyBoxes[i].setInActive();
			}

			for(int i = 0; i < MAX_BULLETS; i++){
				bullets[i].setInActive();
			}

			level1 = false;
			accuracyBonus += 100 * (shotsHit/shotsFired);
			shotsHit = 0;
			shotsFired = 0;
			onLevelTwoMenu = true;
			enemyKillCount = 0;
			playerObject.resetHealth();
		}


		//Get Camera viewMatrix
		mView = camera.getViewMatrix();
		mProj = camera.getProjectionMatrix();
		camera.update(dt);
	}
	if(onHighscoreScreen) {

		if(GetAsyncKeyState(VK_RETURN) & 0x8000 && shotTimeBuffer.waitTimeIsOver()){
			for(int i = 0; i < MAX_ENEMIES; i++){
				enemyBoxes[i].setInActive();
			}

			for(int i = 0; i < MAX_BULLETS; i++){
				bullets[i].setInActive();
			}
			gameOver = false;
			didWin = false;
			onLevelOneMenu = true;
			onHighscoreScreen = false;
			score = 0;
			accuracyBonus = 0;
			level2Timer = 30.0f;
			level3Timer = 45.0f;
			level2 = false;
			level3 = false;

			camera.init(cameraPos, camera.getDirection(), INITIAL_LOOKAT);


			shotTimeBuffer.resetClock(0.0);
		}
	}

	if(gameOver){
		if(GetAsyncKeyState(VK_RETURN) & 0x8000){
			for(int i = 0; i < MAX_ENEMIES; i++){
				enemyBoxes[i].setInActive();
			}

			for(int i = 0; i < MAX_BULLETS; i++){
				bullets[i].setInActive();
			}
			shotTimeBuffer.resetClock(1.0);
			onHighscoreScreen = true;
			enemyKillCount = 0;
			gunSelector = GUNS::NORMAL;
		}
	}


	if(onLevelOneMenu){
		if(GetAsyncKeyState(VK_RETURN) & 0x8000){
			level1 = true;
			onLevelOneMenu = false;
			gunSelector == GUNS::NORMAL;
		}
	}
	if(onLevelTwoMenu){
		if(GetAsyncKeyState(VK_RETURN) & 0x8000){
			level2 = true;
			onLevelTwoMenu = false;
		}
	}
	if(onLevelThreeMenu){
		if(GetAsyncKeyState(VK_RETURN) & 0x8000){
			level3 = true;
			onLevelThreeMenu = false;
		}
	}
	if(level2){
		if(!gameOver)
			level2Timer -= dt;
		if(level2Timer <= 0){
			for(int i = 0; i < MAX_ENEMIES; i++){
				enemyBoxes[i].setInActive();
			}

			for(int i = 0; i < MAX_BULLETS; i++){
				bullets[i].setInActive();
			}
			level2 = false;
			playerObject.resetHealth();
			accuracyBonus += 100 * (shotsHit/shotsFired);
			shotsHit = 0;
			shotsFired = 0;
			onLevelThreeMenu = true;
		}
	}
	if(level3){
		if(!gameOver)
			level3Timer -= dt;
		if(level3Timer <= 0){
			for(int i = 0; i < MAX_ENEMIES; i++){
				enemyBoxes[i].setInActive();
			}

			for(int i = 0; i < MAX_BULLETS; i++){
				bullets[i].setInActive();
			}
			playerObject.resetHealth();
			accuracyBonus += 100 * (shotsHit/shotsFired);
			//score += accuracyBonus;
			scoreAndBonus = score + accuracyBonus;
			shotsHit = 0;
			shotsFired = 0;
			gameOver = true;
			didWin = true;
			level3 = false;

			storeScore();
		}
	}

}


void ColoredCubeApp::drawScene()
{
	D3DApp::drawScene();



	Matrix centerTranslate; // This moves all of the geometry so that 
	Translate(&centerTranslate, -19,0,-19);

	// Restore default states, input layout and primitive topology 
	// because mFont->DrawText changes them.  Note that we can 
	// restore the default states by passing null.
	md3dDevice->OMSetDepthStencilState(0, 0);
	float blendFactors[] = {0.0f, 0.0f, 0.0f, 0.0f};
	md3dDevice->OMSetBlendState(0, blendFactors, 0xffffffff);

	md3dDevice->IASetInputLayout(InputLayout::PosTangentNormalTex);
	//md3dDevice->IASetInputLayout(mVertexLayout);
	md3dDevice->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//experimenting with normals
	mfxNormalMapVar->SetResource(defaultNormalMapRV);
	mfxCubeMapVar->SetResource(mEnvMapRV);

	// Set per frame constants.
	mfxEyePosVar->SetRawValue(&camera.getPosition(), 0, sizeof(D3DXVECTOR3));
	mfxFlashLightVar ->SetRawValue(&mLights[0], 0, sizeof(Light));
	mfxFlashLightVar2->SetRawValue(&mLights[1], 0, sizeof(Light));
	mfxFlashLightVar3->SetRawValue(&mLights[2], 0, sizeof(Light));
	mfxFlashLightVar4->SetRawValue(&mLights[3], 0, sizeof(Light));
	mfxFlashLightVar5->SetRawValue(&mLights[4], 0, sizeof(Light));

	mfxLightType->SetInt(mLightType);
	mfxDiffuseMapVar->SetResource(mDiffuseMapRV);
	mfxSpecMapVar->SetResource(mSpecMapRV);

	// set constants
	mWVP = mView*mProj;
	mfxWVPVar->SetMatrix((float*)&mWVP);

	D3DXMATRIX texMtx;
	D3DXMatrixIdentity(&texMtx);
	mfxTexMtxVar->SetMatrix((float*)&texMtx);


	D3D10_TECHNIQUE_DESC techDesc;
	mTech->GetDesc( &techDesc );


	for(UINT p = 0; p < techDesc.Passes; ++p)
	{
		mTech->GetPassByIndex( p )->Apply(0);
	}

	mfxDiffuseMapVar->SetResource(cityTex);
	mfxNormalMapVar->SetResource(mfxBuildingNormalMapRV);

	if(!gameOver){

		if(level1){
			for (int i = 0; i < 12; i++)
			{
				mWVP = blockerBlocks[i].getWorldMatrix() * centerTranslate * mView*mProj;
				mfxWVPVar->SetMatrix((float*)&mWVP);
				mfxWorldVar->SetMatrix((float*)&blockerBlocks[i].getWorldMatrix());
				blockerBlocks[i].setMTech(mTech);
				blockerBlocks[i].draw();
			}
		}

		for(int i = 0; i < blockCounter; i++){
			mWVP = mazeBlocks[i].getWorldMatrix() * centerTranslate *mView*mProj;
			mfxWVPVar->SetMatrix((float*)&mWVP);
			mfxWorldVar->SetMatrix((float*)&mazeBlocks[i].getWorldMatrix());
			mazeBlocks[i].setMTech(mTech);
			mazeBlocks[i].draw();
		}

		//mfxNormalMapVar->SetResource(defaultNormalMapRV);

		mfxDiffuseMapVar->SetResource(tallTex);
		for(int i = 0; i < blockCounterTall; i++){
			mWVP = mazeBlocksTall[i].getWorldMatrix() * centerTranslate *mView*mProj;
			mfxWVPVar->SetMatrix((float*)&mWVP);
			mfxWorldVar->SetMatrix((float*)&mazeBlocksTall[i].getWorldMatrix());
			mazeBlocksTall[i].setMTech(mTech);
			mazeBlocksTall[i].draw();
		}

		mfxDiffuseMapVar->SetResource(tallerTex);
		for(int i = 0; i < blockCounterTaller; i++){
			mWVP = mazeBlocksTaller[i].getWorldMatrix() * centerTranslate *mView*mProj;
			mfxWVPVar->SetMatrix((float*)&mWVP);
			mfxWorldVar->SetMatrix((float*)&mazeBlocksTaller[i].getWorldMatrix());
			mazeBlocksTaller[i].setMTech(mTech);
			mazeBlocksTaller[i].draw();
		}


		int ghostColor = 1;
		mfxDiffuseMapVar->SetResource(ghostTex);
		mfxNormalMapVar->SetResource(defaultNormalMapRV);
		for (int i = 0; i < MAX_ENEMIES; i++)
		{
			ghostColor = enemyBoxes[i].getHealth();
			mfxFlipGhostVar->SetRawValue(&ghostColor, 0, sizeof(int));


			mWVP = enemyBoxes[i].getWorldMatrix() * centerTranslate *mView*mProj;
			mfxWVPVar->SetMatrix((float*)&mWVP);
			mfxWorldVar->SetMatrix((float*)&enemyBoxes[i].getWorldMatrix());
			enemyBoxes[i].setMTech(mTech);
			enemyBoxes[i].draw();

		}

		ghostColor = 0;
		mfxFlipGhostVar->SetRawValue(&ghostColor, 0, sizeof(int));

		///////NEED TO SET A NEW TEXTURE HERE
		mfxDiffuseMapVar->SetResource(fireTex);

		////////////////////////////////////

		int bulletColor = 0;

		if (gunSelector == GUNS::BURST)
			bulletColor = 1;
		else if (gunSelector == GUNS::POWER)
			bulletColor = 2;

		for (int i = 0; i < MAX_BULLETS; i++)
		{
			mfxBulletColorVar->SetRawValue(&bulletColor, 0, sizeof(int));

			mWVP = bullets[i].getWorldMatrix() * centerTranslate *mView*mProj;
			mfxWVPVar->SetMatrix((float*)&mWVP);
			mfxWorldVar->SetMatrix((float*)&bullets[i].getWorldMatrix());
			bullets[i].setMTech(mTech);
			bullets[i].draw();
		}

		for (int i = 0; i < MAX_NUM_PARTICLES; i++)
		{
			mWVP = particles[i].getWorldMatrix() * centerTranslate * mView*mProj;
			mfxWVPVar->SetMatrix((float*)&mWVP);
			mfxWorldVar->SetMatrix((float*)&particles[i].getWorldMatrix());
			particles[i].setMTech(mTech);
			particles[i].draw();
		}

		bulletColor = 0;
		mfxBulletColorVar->SetRawValue(&bulletColor, 0, sizeof(int));

		mfxDiffuseMapVar->SetResource(grassTex);


		mWVP = alertBlocks[0].getWorldMatrix() * centerTranslate * camera.getMatrixYaw() * mView*mProj;
		mfxWVPVar->SetMatrix((float*)&mWVP);
		mfxWorldVar->SetMatrix((float*)&alertBlocks[0].getWorldMatrix());
		alertBlocks[0].setMTech(mTech);
		alertBlocks[0].draw();

		Matrix rotateAlert;
		RotateY(&rotateAlert, ToRadian(-30));

		Matrix rotateAlert1;
		RotateY(&rotateAlert1, ToRadian(30));

		mWVP = alertBlocks[1].getWorldMatrix() * centerTranslate * camera.getMatrixYaw() * rotateAlert * mView*mProj;
		mfxWVPVar->SetMatrix((float*)&mWVP);
		mfxWorldVar->SetMatrix((float*)&alertBlocks[1].getWorldMatrix());
		alertBlocks[1].setMTech(mTech);
		alertBlocks[1].draw();

		mWVP = alertBlocks[2].getWorldMatrix() * centerTranslate * camera.getMatrixYaw() * rotateAlert1 * mView*mProj;
		mfxWVPVar->SetMatrix((float*)&mWVP);
		mfxWorldVar->SetMatrix((float*)&alertBlocks[2].getWorldMatrix());
		alertBlocks[2].setMTech(mTech);
		alertBlocks[2].draw();

		mfxDiffuseMapVar->SetResource(roadTex);
		mfxNormalMapVar->SetResource(mfxBuildingNormalMapRV);

		mWVP = floor.getWorldMatrix() * centerTranslate *mView*mProj;
		mfxWVPVar->SetMatrix((float*)&mWVP);
		mfxWorldVar->SetMatrix((float*)&floor.getWorldMatrix());
		floor.setMTech(mTech);
		floor.draw();

		mfxDiffuseMapVar->SetResource(keyTex);
		mfxNormalMapVar->SetResource(defaultNormalMapRV);

		int playerHealthVar = playerObject.getHealth();
		mfxFlipPlayerVar->SetRawValue(&playerHealthVar, 0, sizeof(int));
		mWVP = playerObject.getWorldMatrix() * centerTranslate * camera.getMatrixYaw() * mView *mProj;
		mfxWVPVar->SetMatrix((float*)&mWVP);
		mfxWorldVar->SetMatrix((float*)&playerObject.getWorldMatrix());
		playerObject.setMTech(mTech);
		playerObject.draw();


		playerHealthVar = 0;
		mfxFlipPlayerVar->SetRawValue(&playerHealthVar, 0, sizeof(int));
		// We specify DT_NOCLIP, so we do not care about width/height of the rect.
	}
	mSky.draw(camera);

	std::wostringstream newString; 

	if(onHighscoreScreen){
		newString << "Score:  ";
		newString << scoreAndBonus;
		newString << "\n";
	}
	else{
		newString << "Score:  ";
		newString << score;
		newString << "\n";
	}

	if(level1){
		newString << "Killed:  ";
		newString << enemyKillCount;
		newString << "\n";
	}
	if(level2){
		newString.setf(std::ios::fixed);
		newString.precision(0);

		newString << "Timer:  ";
		newString << level2Timer;
		newString << "\n";
		std::wostringstream bigString; 
		std::wostringstream gunString; 
		bigString.setf(std::ios::fixed);
		bigString.precision(0);
		bigString << "";
		D3DXCOLOR faded = D3DXCOLOR(1.0f,1.0f,1.0f, 0.2f);
		if(level2Timer < 5.5){ 
			bigString << level2Timer;
		}

		if (viewGunChanged) {
			if (gunSelector == GUNS::BURST)
				gunString << "BURST";

			else if (gunSelector == GUNS::NORMAL)
				gunString << "NORMAL";
		}

		finalScore = bigString.str();
		RECT R1 = {340, 190, 0, 0};
		scoreFont->DrawText(0, finalScore.c_str(), -1, &R1, DT_NOCLIP, faded);

		gunType = gunString.str();
		RECT R2 = {150, 190, 0, 0};
		gunFont->DrawText(0, gunType.c_str(), -1, &R2, DT_NOCLIP, faded);
	}
	if(level3){
		newString.setf(std::ios::fixed);
		newString.precision(0);

		newString << "Timer:  ";
		newString << level3Timer;
		newString << "\n";

		std::wostringstream bigString; 
		std::wostringstream gunString; 
		bigString.setf(std::ios::fixed);
		bigString.precision(0);
		bigString << "";
		D3DXCOLOR faded = D3DXCOLOR(1.0f,1.0f,1.0f, 0.2f);
		if(level3Timer < 5.5){ 
			bigString << level3Timer;
		}

		if (viewGunChanged) {
			if (gunSelector == GUNS::BURST)
				gunString << "BURST";

			else if (gunSelector == GUNS::NORMAL)
				gunString << "1SHOT";

			else if (gunSelector == GUNS::POWER)
				gunString << "POWER";
		}

		finalScore = bigString.str();
		RECT R1 = {340, 190, 0, 0};
		scoreFont->DrawText(0, finalScore.c_str(), -1, &R1, DT_NOCLIP, faded);

		gunType = gunString.str();
		RECT R2 = {150, 190, 0, 0};
		gunFont->DrawText(0, gunType.c_str(), -1, &R2, DT_NOCLIP, faded);
	}

	finalScore = newString.str();
	RECT R1 = {25, 15, 0, 0};
	mFont->DrawText(0, finalScore.c_str(), -1, &R1, DT_NOCLIP, WHITE);


	RECT R = {5, 5, 0, 0};
	md3dDevice->RSSetState(0);
	mFont->DrawText(0, mFrameStats.c_str(), -1, &R, DT_NOCLIP, WHITE);

	if(gameOver && !didWin && !onHighscoreScreen){
		std::wostringstream gameOver;

		gameOver << "          Game Over!\n";
		gameOver << "\n     Accuracy Bonus: ";
		gameOver << accuracyBonus;
		gameOver << "\n            Score:  ";
		gameOver << scoreAndBonus;
		gameOver << "\n Press Enter to Continue";

		gameOverString = gameOver.str();
		RECT R1 = {150, 150, 0, 0};
		endFont->DrawText(0, gameOverString.c_str(), -1, &R1, DT_NOCLIP, WHITE);
	}
	if(gameOver && didWin && !onHighscoreScreen){
		std::wostringstream gameOver;

		gameOver << "             You Win!\n";
		gameOver << "\n    Accuracy Bonus: ";
		gameOver << accuracyBonus;
		gameOver << "\n             Score:  ";
		gameOver << scoreAndBonus;
		gameOver << "\n Press Enter to Continue";

		gameOverString = gameOver.str();
		RECT R1 = {150, 150, 0, 0};
		endFont->DrawText(0, gameOverString.c_str(), -1, &R1, DT_NOCLIP, WHITE);
	}
	if (onHighscoreScreen) {
		std::wostringstream highScoreText;
		highScoreText << "            Your Score:  ";
		highScoreText << scoreAndBonus;
		highScoreText << "\n        Highscores: \n";

		highScoreText << getTopThreeHighScores().c_str();

		highScoreText << "\n Press Enter to Restart";

		gameOverString = highScoreText.str();
		RECT R1 = {150, 150, 0, 0};
		endFont->DrawText(0, gameOverString.c_str(), -1, &R1, DT_NOCLIP, WHITE);

	}
	if(onLevelOneMenu){
		std::wostringstream gameOver;

		gameOver << "                          Level 1\n\n";
		gameOver << "          Kill 10 enemies to advance";
		gameOver << "\n                Accuracy Counts!";
		gameOver << "\nGreater Accuracy = Greater Score!";
		gameOver << "\n              Press Enter to Start";

		gameOverString = gameOver.str();
		RECT R1 = {35, 100, 0, 0};

		endFont->DrawText(0, gameOverString.c_str(), -1, &R1, DT_NOCLIP, WHITE);
	}
	if(onLevelTwoMenu){
		std::wostringstream gameOver;

		gameOver << "                  Level 2\n\n";
		gameOver << "    Survive for 30 seconds\n";
		gameOver << "Burst Gun! Toggle: Up Arrow";
		gameOver << "\n      Press Enter to Start";

		gameOverString = gameOver.str();
		RECT R1 = {125, 150, 0, 0};
		endFont->DrawText(0, gameOverString.c_str(), -1, &R1, DT_NOCLIP, WHITE);
	}
	if(onLevelThreeMenu){
		std::wostringstream gameOver;

		gameOver << "                 Level 3\n\n";
		gameOver << " Survive for 45 seconds\n";
		gameOver << " Power Gun! Double Damage";
		gameOver << "\n    Press Enter to Start";

		gameOverString = gameOver.str();
		RECT R1 = {150, 150, 0, 0};
		endFont->DrawText(0, gameOverString.c_str(), -1, &R1, DT_NOCLIP, WHITE);
	}

	mSwapChain->Present(0, 0);
}

//to be made later possibly
void ColoredCubeApp::restartGame() { }

void ColoredCubeApp::buildFX()
{
	//DWORD shaderFlags = D3D10_SHADER_ENABLE_STRICTNESS;
	//#if defined( DEBUG ) || defined( _DBUG )
	//	shaderFlags |= D3D10_SHADER_DEBUG;
	//	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
	//#endif

	/*ID3D10Blob* compilationErrors = 0;
	HRESULT hr = 0;*/


	/*hr = D3DX10CreateEffectFromFile(L"lighting.fx", 0, 0, 
	"fx_4_0", shaderFlags, 0, md3dDevice, 0, 0, &mFX, &compilationErrors, 0);
	if(FAILED(hr))
	{
	if( compilationErrors )
	{
	MessageBoxA(0, (char*)compilationErrors->GetBufferPointer(), 0, 0);
	ReleaseCOM(compilationErrors);
	}
	DXTrace(__FILE__, (DWORD)__LINE__, hr, L"D3DX10CreateEffectFromFile", true);
	} */


	/*mTech = mFX->GetTechniqueByName("LightTech");

	mfxWVPVar    = mFX->GetVariableByName("gWVP")->AsMatrix();
	mfxWorldVar  = mFX->GetVariableByName("gWorld")->AsMatrix();
	mfxEyePosVar = mFX->GetVariableByName("gEyePosW");
	mfxFlashLightVar  = mFX->GetVariableByName("gLight1");
	mfxFlashLightVar2 = mFX->GetVariableByName("gLight2");
	mfxFlashLightVar3 = mFX->GetVariableByName("gLight3");
	mfxFlashLightVar4 = mFX->GetVariableByName("gLight4");
	mfxFlashLightVar5 = mFX->GetVariableByName("gLight5");
	mfxLightType = mFX->GetVariableByName("gLightType1")->AsScalar();
	mfxDiffuseMapVar = mFX->GetVariableByName("gDiffuseMap")->AsShaderResource();
	mfxSpecMapVar    = mFX->GetVariableByName("gSpecMap")->AsShaderResource();
	mfxTexMtxVar     = mFX->GetVariableByName("gTexMtx")->AsMatrix();

	mfxFlipGhostVar = mFX->GetVariableByName("ghostFlip");
	mfxFlipPlayerVar = mFX->GetVariableByName("playerFlip");*/

	mTech             = fx::ColoredCubeFX->GetTechniqueByName("LightTech");
	mfxWVPVar         = fx::ColoredCubeFX->GetVariableByName("gWVP")->AsMatrix();
	mfxWorldVar       = fx::ColoredCubeFX->GetVariableByName("gWorld")->AsMatrix();
	mfxEyePosVar      = fx::ColoredCubeFX->GetVariableByName("gEyePosW");
	mfxFlashLightVar  = fx::ColoredCubeFX->GetVariableByName("gLight1");
	mfxFlashLightVar2 = fx::ColoredCubeFX->GetVariableByName("gLight2");
	mfxFlashLightVar3 = fx::ColoredCubeFX->GetVariableByName("gLight3");
	mfxFlashLightVar4 = fx::ColoredCubeFX->GetVariableByName("gLight4");
	mfxFlashLightVar5 = fx::ColoredCubeFX->GetVariableByName("gLight5");
	mfxLightType      = fx::ColoredCubeFX->GetVariableByName("gLightType1")->AsScalar();
	mfxDiffuseMapVar  = fx::ColoredCubeFX->GetVariableByName("gDiffuseMap")->AsShaderResource();
	mfxSpecMapVar     = fx::ColoredCubeFX->GetVariableByName("gSpecMap")->AsShaderResource();
	mfxTexMtxVar      = fx::ColoredCubeFX->GetVariableByName("gTexMtx")->AsMatrix();


	mfxNormalMapVar      = fx::ColoredCubeFX->GetVariableByName("gNormalMap")->AsShaderResource();
	mfxCubeMapVar        = fx::ColoredCubeFX->GetVariableByName("gCubeMap")->AsShaderResource();


	mfxFlipGhostVar =  fx::ColoredCubeFX->GetVariableByName("ghostFlip");
	mfxFlipPlayerVar = fx::ColoredCubeFX->GetVariableByName("playerFlip");
	mfxBulletColorVar =  fx::ColoredCubeFX->GetVariableByName("bulletColor");
}

void ColoredCubeApp::buildVertexLayouts()
{
	// Create the vertex input layout.
	//D3D10_INPUT_ELEMENT_DESC vertexDesc[] =
	//{
	//	{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 0,  D3D10_INPUT_PER_VERTEX_DATA, 0},
	//	{"NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0},
	//	{"DIFFUSE",  0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D10_INPUT_PER_VERTEX_DATA, 0},
	//	{"SPECULAR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 40, D3D10_INPUT_PER_VERTEX_DATA, 0},
	//	{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,       0, 24, D3D10_INPUT_PER_VERTEX_DATA, 0},
	//};
	//
	//// Create the input layout
	//D3D10_PASS_DESC PassDesc;
	//mTech->GetPassByIndex(0)->GetDesc(&PassDesc);
	//HR(md3dDevice->CreateInputLayout(vertexDesc, 5, PassDesc.pIAInputSignature,
	//	PassDesc.IAInputSignatureSize, &mVertexLayout));
}

void ColoredCubeApp::runExplosion(Vector3 pos){
	for (int i = 0; i < MAX_NUM_PARTICLES; i++)
	{
		particles[i].setActive();

		float rand1 = (rand() % 10);
		if(rand() % 2 == 0) rand1 *= -1;
		float rand2 = (rand() % 10);
		if(rand() % 2 == 0) rand2 *= -1;
		float rand3 = (rand() % 10);
		if(rand() % 2 == 0) rand3 *= -1;

		particles[i].setPosition(pos);
		particles[i].setVelocity(D3DXVECTOR3(rand1,rand2,rand3));

	}
	explosionRunning = true;
}


bool ColoredCubeApp::isTowardTop(){
	//position the bullet infront of the player
	Vector3 direction = Vector3(-1,0,0);
	Transform(&direction, &direction, &camera.getMatrixYaw()); 

	//Vector3 fireDirection = camera.getLookAt(); /*- playerObject.getPosition();*/
	Normalize(&direction, &direction);

	if (direction.x < -0.7 && (direction.z > -0.7f && direction.z < 0.7f)){
		//alertBlocks[0].setActive();
		return true;
	}
	else{
		//alertBlocks[0].setInActive();
		return false;
	}
}
bool ColoredCubeApp::isTowardLeft(){
	//position the bullet infront of the player
	Vector3 direction = Vector3(-1,0,0);
	Transform(&direction, &direction, &camera.getMatrixYaw()); 

	//Vector3 fireDirection = camera.getLookAt(); /*- playerObject.getPosition();*/
	Normalize(&direction, &direction);

	if (direction.z < -0.7 && (direction.x > -0.7f && direction.x < 0.7f)){
		//alertBlocks[0].setActive();
		return true;
	}
	else{
		//alertBlocks[0].setInActive();
		return false;
	}
}
bool ColoredCubeApp::isTowardRight(){
	//position the bullet infront of the player
	Vector3 direction = Vector3(-1,0,0);
	Transform(&direction, &direction, &camera.getMatrixYaw()); 

	//Vector3 fireDirection = camera.getLookAt(); /*- playerObject.getPosition();*/
	Normalize(&direction, &direction);

	if (direction.z > 0.7 && (direction.x > -0.7f && direction.x < 0.7f)){
		//alertBlocks[0].setActive();
		return true;
	}
	else{
		//alertBlocks[0].setInActive();
		return false;
	}
}
bool ColoredCubeApp::isTowardBottom(){
	//position the bullet infront of the player
	Vector3 direction = Vector3(-1,0,0);
	Transform(&direction, &direction, &camera.getMatrixYaw()); 

	//Vector3 fireDirection = camera.getLookAt(); /*- playerObject.getPosition();*/
	Normalize(&direction, &direction);

	if (direction.x > 0.7 && (direction.z > -0.7f && direction.z < 0.7f)){
		//alertBlocks[0].setActive();
		return true;
	}
	else{
		//alertBlocks[0].setInActive();
		return false;
	}
}

bool ColoredCubeApp::isGhostInTop(){

	for(int i = 0; i < MAX_ENEMIES; i++){
		if(enemyBoxes[i].getActiveState()){
			Vector3 ghostPos = enemyBoxes[i].getPosition();

			if(ghostPos.x < 18 && (ghostPos.z > 16 && ghostPos.z < 21)){
				//alertBlocks[0].setActive();
				return true;
			}
		}
	}

	//alertBlocks[0].setInActive();
	return false;

}
bool ColoredCubeApp::isGhostInRight(){

	for(int i = 0; i < MAX_ENEMIES; i++){
		if(enemyBoxes[i].getActiveState()){
			Vector3 ghostPos = enemyBoxes[i].getPosition();

			if(ghostPos.z > 20 && (ghostPos.x > 16 && ghostPos.x < 21)){
				//alertBlocks[0].setActive();
				return true;
			}
		}
	}

	//alertBlocks[0].setInActive();
	return false;

}
bool ColoredCubeApp::isGhostInBottom(){

	for(int i = 0; i < MAX_ENEMIES; i++){
		if(enemyBoxes[i].getActiveState()){
			Vector3 ghostPos = enemyBoxes[i].getPosition();

			if(ghostPos.x > 20 && (ghostPos.z > 16 && ghostPos.z < 21)){
				//alertBlocks[0].setActive();
				return true;
			}
		}
	}

	//alertBlocks[0].setInActive();
	return false;

}
bool ColoredCubeApp::isGhostInLeft(){

	for(int i = 0; i < MAX_ENEMIES; i++){
		if(enemyBoxes[i].getActiveState()){
			Vector3 ghostPos = enemyBoxes[i].getPosition();

			if(ghostPos.z < 18 && (ghostPos.x > 16 && ghostPos.x < 21)){
				//alertBlocks[0].setActive();
				return true;
			}
		}
	}

	//alertBlocks[0].setInActive();
	return false;

}

void ColoredCubeApp::setAlerts(){
	if(isTowardTop()){
		// Check for ghost behind
		if(isGhostInBottom()) alertBlocks[0].setActive();
		else alertBlocks[0].setInActive();
		//Check for ghost to left
		if(isGhostInLeft()) alertBlocks[1].setActive();
		else alertBlocks[1].setInActive();
		//Check for ghost to right
		if(isGhostInRight()) alertBlocks[2].setActive();
		else alertBlocks[2].setInActive();
	}
	else if(isTowardLeft()){
		// Check for ghost behind
		if(isGhostInRight()) alertBlocks[0].setActive();
		else alertBlocks[0].setInActive();
		//Check for ghost to left
		if(isGhostInBottom()) alertBlocks[1].setActive();
		else alertBlocks[1].setInActive();
		//Check for ghost to right
		if(isGhostInTop()) alertBlocks[2].setActive();
		else alertBlocks[2].setInActive();
	}
	else if(isTowardBottom()){
		// Check for ghost behind
		if(isGhostInTop()) alertBlocks[0].setActive();
		else alertBlocks[0].setInActive();
		//Check for ghost to left
		if(isGhostInRight()) alertBlocks[1].setActive();
		else alertBlocks[1].setInActive();
		//Check for ghost to right
		if(isGhostInLeft()) alertBlocks[2].setActive();
		else alertBlocks[2].setInActive();
	}
	else if(isTowardRight()){
		// Check for ghost behind
		if(isGhostInLeft()) alertBlocks[0].setActive();
		else alertBlocks[0].setInActive();
		//Check for ghost to left
		if(isGhostInTop()) alertBlocks[1].setActive();
		else alertBlocks[1].setInActive();
		//Check for ghost to right
		if(isGhostInBottom()) alertBlocks[2].setActive();
		else alertBlocks[2].setInActive();
	}

}

#include <fstream>
#include <vector>
#include <algorithm>
void ColoredCubeApp::storeScore() {
	std::ofstream out("highscores.txt", std::ofstream::app);
	out << scoreAndBonus << std::endl;
	out.close();
}

string ColoredCubeApp::getTopThreeHighScores() {
	std::ifstream fin("highscores.txt");
	std::vector<int> v;
	int dummy;
	while (fin >> dummy)
		v.push_back(dummy);
	fin.close();
	std::sort (v.begin(), v.end());

	string output = "";

	for (int i = 0; i < 3 && i < v.size(); i++)
	{
		output += std::to_string(i+1) + ". " + std::to_string(v[v.size() - 1 - i]) + "\n"; 
	}

	return output;
}