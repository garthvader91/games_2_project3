
#pragma once


#include "d3dUtil.h"
#include "GameObject.h"
#include "Box.h"
#include "constants.h"
#include "point.h"
#include "Turrent.h"

class PlayerObject : public GameObject
{
public:
	PlayerObject();
	void init(Turrent *b, float r, Vector3 pos, Vector3 vel, float sp, float s);
	void update(float dt);

	void draw();


	void bounce(GameObject* go);
	void setCollision(bool b) { collision = b; }

	bool getKey(){return hasKey;}
	void setKey(bool k){hasKey = k;}

	Vector3 originalPos;
	void decrementHealth() { health--; }
	void resetHealth() { health = 3; }
	int getHealth() { return health; }
private:
	Turrent* turrent;
	int health;
	bool collision;
	bool hasKey;
};