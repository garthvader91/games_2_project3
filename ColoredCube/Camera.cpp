#include "Camera.h"

Camera::Camera()
{
	speed = 5;
	FoV = 0.4*PI;
	aspectRatio = 480.0f/640.0f;
	nearClippingPlane = 0.1f;
	farClippingPlane = 1000.0f;
	up = Vector3(0.0f, 1.0f, 0.0f);
	position = Vector3(0,0,0);
	lookAt = Vector3(0.0f, 0.0f, 0.0f);
	refDirection = Vector3(1,0,0);
	yaw = 0;
	roll = 0;
	pitch = 0;
	rightDownLastFrame = false;
	leftDownLastFrame = false;
	footstepsCycle = 0;
	stoppedWalking = true;
	rotated = false;
}

Camera::~Camera()
{
	//nothing to deallocate
}

void Camera::init(Vector3 pos, Vector3 dir, Vector3 look)
{
	position = pos;
	direction = dir;
	lookAt = look;
	yaw = 0;
	roll = 0;
	pitch = 0;
	up = Vector3(0.0f, 1.0f, 0.0f);

}

void Camera::setPerspective()
{
	D3DXMatrixPerspectiveFovLH(&mProj, FoV, aspectRatio, nearClippingPlane,farClippingPlane); 
}
void Camera::update(float dt)
{
	position = Vector3(0,0,0);
	lookAt = Vector3(0,0,0);
	pitch = 0;

	rotated = false;
	float deltaYaw = 0;
	float _speed = 175;
	float deltaPitch = 0;

	direction = Vector3(0,0,0);
	Matrix yawR;
	Matrix pitchR;
	Matrix rollR;

	Identity(&yawR);
	Identity(&pitchR);
	Identity(&rollR);
	

	if(GetAsyncKeyState(VK_RIGHT) & 0x8000)
	{
		deltaYaw += _speed*dt;
		yaw += deltaYaw;
		rotated = true;
	}
	if(GetAsyncKeyState(VK_LEFT) & 0x8000)
	{
		rotated = true;
		deltaYaw -= _speed*dt;
		yaw+= deltaYaw;
	}

	// old camera position added as offset
	/*if (GetAsyncKeyState(VK_UP) & 0x8000)
	{
		rotated = true;
		deltaPitch += _speed*dt;
		if (deltaPitch > 1) 
			deltaPitch = 1;
		if (pitch <= 30){
			pitch += deltaPitch;
		}
	}
	if (GetAsyncKeyState(VK_DOWN) & 0x8000)
	{
		rotated = true;
		deltaPitch -= _speed*dt;
		if (deltaPitch < -1) 
			deltaPitch = -1;
		if (pitch >= -30){
			pitch += deltaPitch;
		}

	}*/

	RotateY(&yawR, ToRadian(yaw));
	RotateZ(&pitchR, ToRadian(pitch));

	Vector3 transformedRef(0,0,1);
	Matrix temp;
	//D3DXMatrixMultiply(&temp,&pitchR, &yawR);
	//temp = temp * pitchR;
	//Transform(&transformedRef, &transformedRef, &temp);
	//D3DXVec3Normalize(&transformedRef, &transformedRef);
	//lookAt = transformedRef * 10;

	bool walking = false;

	//if(GetAsyncKeyState('A') & 0x8000)
	//{
	//	direction.z = 1;
	//	walking = true;
	//}
	//if(GetAsyncKeyState('D') & 0x8000)
	//{
	//	direction.z = -1;
	//	walking = true;
	//}
	//if(GetAsyncKeyState('S') & 0x8000)
	//{
	//	if(position.x >= 0){
	//		direction.x = -1;
	//		walking = true;
	//	}
	//}
	//if(GetAsyncKeyState('W') & 0x8000)
	//{
	//	direction.x = 1;
	//	walking = true;
	//}

	//D3DXVec3Normalize(&direction, &direction);
	//Transform(&direction, &direction, &yawR);

	if (ToRadian(yaw) >= 360 || ToRadian(yaw) <= -360) {	yaw = 0;}

	Matrix spin;
	RotateY(&spin, ToRadian(yaw));
	Matrix translate;
	Translate(&translate, 2.7, 1.8, 1);
	Matrix transform = translate * spin;

	Transform(&position, &position, &transform);

	
	//_RPT1(0,"lookAt x %f " , lookAt.x);
	//_RPT1(0,"lookAt z %f\n", lookAt.z);

	//position += direction*speed*dt;

	/*Vector3 transformedRef(1,0,0);
	Matrix temp;
	D3DXMatrixMultiply(&temp,&pitchR, &yawR);*/

	//if (rotated) {
	//	Transform(&transformedRef, &transformedRef, &temp);
	//	D3DXVec3Normalize(&transformedRef, &transformedRef);
	//	lookAt = transformedRef * 10;
	//	lookAt += position;
	//	rotated = false;
	//}
	//else {
	//	//lookAt += direction*speed*dt;
	//}

	//Generate new matrix
	D3DXMatrixLookAtLH(&mView, &position, &lookAt, &up);

	

}