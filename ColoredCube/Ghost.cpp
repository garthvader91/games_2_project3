#include "Ghost.h"

void Ghost::init(TexBox *b, float r, Vector3 pos, Vector3 vel, float sp, float s, PlayerObject* player)
{
	GameObject::init(b, r, pos, vel, sp, s);
	direction = Vector3(0,0,0);
	this->player = player;
	health = 2;
}

Ghost::Ghost() {

}

void Ghost::update(float dt){
	//RotateY(&GameObject::getWorldMatrix(), angle);
	//updatePatterns(dt);
	if ( GameObject::getActiveState()) {
		vectorTrack();
		if (abs(direction.z) > abs(direction.x)) {
			if (direction.z < 0) direction.z = -1; else direction.z = 1;
			direction.x = 0;
		}
		else {
			if (direction.x < 0) direction.x = -1; else direction.x = 1;
			direction.z = 0;
		}

		GameObject::setVelocity(direction * GameObject::getSpeed());
		GameObject::update(dt);

	}

}


void Ghost::vectorTrack() {

	direction = player->getPosition() - GameObject::getPosition();
	if(direction.x == 0 && direction.z == 0)
		return;

	D3DXVec3Normalize(&direction, &direction);
}


void Ghost::bounce(GameObject *go) {
	Vector3 collisionVector = go->getPosition() - getPosition();

	//normalize the collision vec and make it super small so that it's not shaky
	collisionVector = *D3DXVec3Normalize(&collisionVector, &collisionVector)/200;

	int count = 30;
	do {
		setPosition(getPosition() - collisionVector);
		count--;
	} while (GameObject::collided(go) && count);
}
